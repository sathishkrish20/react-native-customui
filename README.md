# react-native-razorpay

See this Video for Easy integration for IOS ->  https://www.youtube.com/watch?time_continue=4&v=hE-F0QqTwnI   
IOS

1. Open `path/to/your/project/ios/<your_project>.xcworkspace` or
`path/to/your/project/ios/<your_project>.xcodeproj` on Xcode 

2. go to nodemodules/react-native-customui/ios/

3. Drag and drop the nodemodules/react-native-customui/ios/Razorpay.xcodeprj into Project(under Libraries) on Xcode 

4. expand Razorpay.xcodeprj and products 

5. Drag and Drop libRazorPay.a to Linked Frameworks and libraries (click on the medflic(app Name)  General-> Linked Frameworks and libraries)

6. Click on the + button on Embeded Binaries( Just above the Linked Frameworks and libraries) click other and select nodemodules/react-nativecustomui/ios/Razorpay.framework

7. go to build phases, check for "Always Embed Swift Standard Libraries" and changed to Yes


Android

2. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.razorpay.rn.RazorpayPackage;` to the imports at the top of
  the file
  - Add `new RazorpayPackage()` to the list returned by the `getPackages()` method
3. Append the following lines to `android/settings.gradle`:
    ```gradle
    include ':react-native-customui'
    project(':react-native-customui').projectDir = new File(rootProject.projectDir,   '../node_modules/react-native-customui/android')
    ```
4. Insert the following lines inside the dependencies block in
`android/app/build.gradle`:
    ```gradle
    
    implementation "com.facebook.react:react-native:0.58.6"
    compile (name: "razorpay-android", ext: 'aar')
    implementation project(':react-native-customui')
    ```

5. Add following line to `package.json` `dependencies`
   ```
    "react-native-customui": "./../react-native-customui"
   ```
   here `./../react-native-customui` is relative path to `react-native-customui` directory, it can be diffrent in your case.

6. Copy `razorpay-android.aar` from `react-native-customui/android/libs/razorpay-android.aar` and add to `android/app/libs` directory.




React Native wrapper around our CustomUI SDK

To know more about Razorpay payment flow and steps involved, please read up here:
<https://docs.razorpay.com/v1/page/android-custom-ui-sdk-v2x>
<https://docs.razorpay.com/v1/page/ios-custom-ui-sdk>


## Manual installation

Try below given steps or refer `example` directory for reference.

### Note:

Please install the latest version of react-native i.e 0.57.1 and react version 16.5.0

### Note: 
This release is meant for Xcode 10. We strongly recommend you to wait till the stable version of react-native that supports Xcode 10 comes out , we have made our fixes but it is still remommended that you wait.You can see here that react-native is still working on a stable Xcode 10 release.Please use your discretion.

https://github.com/facebook/react-native/issues/19573

### Note: 
The iOS framework is shipped with simulator architectures , you have to remove them before you archive, just google  stripping simulator architectures and follow the steps.Also remember to enable bitcode on both your iOS project as well as the RazorpayCheckout project.

### Note : To avoid duplicate module name collisions please copy the example project to a separate folder and try.

### Steps for Linking iOS SDK

React Native creates static library for each plugin / library / framework / native module being used.

Due to some limitation on the way Xcode links static and dynamic
libraries / frameworks to projects, we require some additional
steps to be followed to link our iOS SDK to the React Native project.

Put `react-native-customui` in the relevant directory.
See this Video for Easy integration for IOS ->  https://www.youtube.com/watch?time_continue=4&v=hE-F0QqTwnI   

## Usage

Sample code to integrate with Razorpay can be found in
[index.js][index.js] in the included example directory.

`$ npm i`

### Steps

1. Import Razorpay module to your component:
    ```js
    import Razorpay from 'react-native-customui';
    ```

2. Call `Razorpay.open` method with the payment `options`. The method
returns a **JS Promise** where `then` part corresponds to a successful payment
and the `catch` part corresponds to payment failure.
    ```js
    <TouchableHighlight onPress={() => {
      var options = {
        description: 'Credits towards consultation',
        currency: 'INR',
        key_id: 'rzp_test_1DP5mmOlF5G5ag',
        amount: '5000',
        email: 'void@razorpay.com',
        contact: '9999999123',
        method: 'netbaking',
        bank: 'HDFC'
        
      }
      Razorpay.open(options).then((data) => {
        // handle success
        alert(`Success: ${data.razorpay_payment_id}`);
      }).catch((error) => {
        // handle failure
        alert(`Error: ${error.code} | ${error.description}`);
      });
    }}>
    ```

A descriptive [list of valid options for customui][options] is available [here] : https://docs.razorpay.com/v1/page/android-custom-ui-sdk-v2x

## License

react-native-customui is Copyright (c) 2016 Razorpay Software Pvt. Ltd.
It is distributed under [the MIT License][LICENSE].

We ♥ open source software!
See [our other supported plugins / SDKs][integrations]
or [contact us][contact] to help you with integrations.

[contact]: mailto:integrations@razorpay.com?subject=Help%20with%20React%20Native "Send us a mail"
[index.js]: example/index.js "index.js"
[integrations]: https://razorpay.com/integrations "List of our integrations"
[LICENSE]: /LICENSE "MIT License"
[wiki]: https://github.com/razorpay/react-native-customui/wiki/Manual-installation
