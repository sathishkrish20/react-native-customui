 #!/bin/sh
## Run this script after making changes to the module

react-native unlink react-native-customui
npm uninstall react-native-customui
npm install
react-native link
